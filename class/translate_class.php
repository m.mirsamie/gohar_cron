<?php
    class translate_class{
        public $field_translate = array();
        public function translate($db_row)
        {
            $out = array();
            foreach ($db_row as $key=>$value)
            {
                if(isset($this->field_translate[$key]))
                {
                    $out[$this->field_translate[$key]] = $value;
                }
            }
            $this->extraTranslate($db_row, $out);
            return($out);
        }
        public function extraTranslate($db_row,&$out)
        {
            //Takhfif - public ,....
        }
    }