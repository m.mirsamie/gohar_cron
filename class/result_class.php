<?php
    class result_class{
        public $my ;
        public $connected = FALSE;
        public function __construct($host,$user,$pass,$database) {
            $this->my = new mysqli($host, $user, $pass, $database);
            if($this->my->connect_errno!==FALSE)
            {
                $this->my->set_charset("utf8");
                $this->connected = TRUE;
            }
        }
        public function save($db_row_transalet_out,$table = 'flights')
        {
            if($this->connected)
            {
                $query = array();
                for($i = 0;$i < count($db_row_transalet_out);$i++)
                {
                    $f = array();
                    $v = array();
                    foreach($db_row_transalet_out[$i] as $key=>$value)
                    {
                        $f[] = "`$key`";
                        $v[] = "'$value'";
                    }
                    if(count($f)>0)
                    {
                        $query[] = "insert into `$table` (".  implode(",", $f).") values (".  implode(",", $v).")";
                    }
                }
                if(count($query)>0)
                {
                    $this->my->multi_query(implode(";",$query)) ;
                }
            }
        }
    }
