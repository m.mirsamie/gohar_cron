<?php
    class gohar_dbs_class{
        public $my;
        public $connectd = FALSE;
        public $database = 'admin_cms';
        public $data = array();
        public function __construct($database='') {
            $database = trim($database);
            if($database=='')
            {
                $database = $this->database;
            }
            $conf = new conf();
            $this->my = new mysqli($conf->host, $conf->user, $conf->pass, $database);
            if($this->my->connect_errno!==FALSE)
            {
                $this->my->set_charset("utf8");
                $this->connected = TRUE;
                $result = $this->my->query("SELECT id_gohar,name,site,name_db,user_db,pass_db,server_db,direct_ticket FROM `travel_agency` WHERE `booking`='1' and `id_gohar` > 0");
                while($r = $result->fetch_array())
                {
                    $this->data[] = $r;
                }
            }
        }
    }